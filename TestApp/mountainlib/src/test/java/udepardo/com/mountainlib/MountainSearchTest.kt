package udepardo.com.mountainlib

import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import udepardo.com.mountainlib.instrumentation.SearchInstruments
import udepardo.com.mountainlib.model.BaseModel


@RunWith(RobolectricTestRunner::class)
class MountainSearchTest {

    private lateinit var callback: SearchInstruments.TestCallback

    private val sourceData: List<BaseModel> by lazy {
        Utils.readStream(SearchInstruments.givenAFile().inputStream())
    }

    @Before
    fun before() {
        callback = SearchInstruments.TestCallback()
    }

    @Test
    fun verifyThatCSVFileIsParsed() {
        assert(Utils.readStream(SearchInstruments.givenAFile().inputStream()).isNotEmpty())
    }

    @Test
    fun verifyThatMapWontDropAnyItem() {
        SearchInstruments.givenAMunro(callback).searchValues()
        assert(callback.list?.size == sourceData.size)
    }

    @Test
    fun verifyHillCategoryFiltersMun() {
        val munro = SearchInstruments.givenAMunro(callback)
        munro.searchValues(hillCategory = CATEGORY.MUN)
        assert(callback.isFired)
        assert(callback.list != null && callback.list?.size!! > 0)
        assert(sourceData.filter { it.yNow == "MUN" }.size == callback.list?.size)
    }

    @Test
    fun verifyHillCategoryFiltersTOP() {
        val munro = SearchInstruments.givenAMunro(callback)
        munro.searchValues(hillCategory = CATEGORY.TOP)
        assert(callback.isFired)
        assert(callback.list != null && callback.list?.size!! > 0)
        assert(sourceData.filter { it.yNow == "TOP" }.size == callback.list?.size)
    }

    @Test
    fun verifyThatSortsAlphabetically() {
        val munro = SearchInstruments.givenAMunro(callback)
        munro.searchValues(sort = SORTING.ALPHABETICALLY, sortOrder = ORDER.ASC)
        assert(callback.isFired)
        callback.list?.apply {
            assert(this.isNotEmpty())
            var ordered = true
            for (item: Int in 1 until this.size) {
                if (this[item].name.compareTo(this[item - 1].name) == -1) {
                    ordered = false
                }
            }
            assert(ordered)
        }
    }

    @Test
    fun verifyThatSortsAlphabeticallyInverted() {
        val munro = SearchInstruments.givenAMunro(callback)
        munro.searchValues(sort = SORTING.ALPHABETICALLY, sortOrder = ORDER.DESC)
        assert(callback.isFired)
        callback.list?.apply {
            assert(this.isNotEmpty())
            var ordered = true
            for (item: Int in 1 until this.size) {
                if (this[item].name.compareTo(this[item - 1].name) == 1) {
                    ordered = false
                }
            }
            assert(ordered)
        }
    }

    @Test
    fun verifyHeightFilter() {
        val munro = SearchInstruments.givenAMunro(callback)
        munro.searchValues(minHeight = 1000f, maxHeight = 1200f )
        assert(callback.isFired)
        callback.list?.apply {
            assert(this.isNotEmpty())
            assert(this.none { it.height < 1000f && it.height > 1200f })

        }
    }
}




