package udepardo.com.mountainlib.instrumentation

import udepardo.com.mountainlib.TestSearch
import udepardo.com.mountainlib.model.MountainViewModel
import udepardo.com.mountainlib.model.Utils
import java.io.File

object  SearchInstruments{

    class TestCallback {
        var isFired = false
        var list: List<MountainViewModel>? = null
    }

    fun givenAFile() = File(javaClass.classLoader.getResource("munrotab_v6.2.csv").file)

    private fun givenTestData() = Utils.readStream(SearchInstruments.givenAFile().inputStream())

    fun givenAMunro(callback : TestCallback) = TestSearch(givenTestData()) {
            callback.isFired = true
            callback.list = it
        }

}