package udepardo.com.mountainlib.repository

import android.content.Context
import udepardo.com.mountainlib.Utils
import udepardo.com.mountainlib.interactor.MunroResult
import udepardo.com.mountainlib.model.BaseModel
import java.io.*
import java.lang.Exception
import java.lang.IllegalArgumentException
import java.net.HttpURLConnection
import java.net.URL

/**
 * The local repository needs the context to create/read the file. Ideally we created the file outside this layer and we inject it to the repositories,
 * but as the library is selfsufficient, i decide to inject the context.
 */
class LocalRepository(val context: Context) : LocalRepo {
    @Throws(FileNotFoundException::class)
    override fun getMountains(): MunroResult<List<BaseModel>> {
        val fis = context.openFileInput("customData.munro")
        return try {
            val inputStream = ObjectInputStream(fis)
            val resultData = inputStream.readObject() as ArrayList<BaseModel>
            inputStream.close()
            fis.close()
            return MunroResult(resultData)
        } catch (ioe: Exception) {
            MunroResult(error = ioe)
        } finally {
            fis.close()
        }
    }

    override fun saveMountains(data: List<BaseModel>): MunroResult<Unit> {
        return try {
            val fos = context.openFileOutput("customData.munro", Context.MODE_PRIVATE)
            val os = ObjectOutputStream(fos)
            os.writeObject(data)
            os.close()
            fos.close()
            MunroResult(Unit)
        } catch (iae: IllegalArgumentException) {
            MunroResult(error = iae)
        } catch (fne: FileNotFoundException) {
            MunroResult(error = fne)
        } catch (ioe: IOException) {
            MunroResult(error = ioe)
        }
    }
}

class RemoteRepository : RemoteRepo{
    override fun getMountains(): MunroResult<List<BaseModel>> {
        try {
            val url = URL("https://bicicoru.herokuapp.com/test")
            (url.openConnection() as HttpURLConnection).apply {
                return if (this.responseCode == HttpURLConnection.HTTP_OK) {
                    MunroResult(data = Utils.readStream(this.inputStream))
                } else {
                    MunroResult(error = IOException("Error getting data from network"))
                }
            }
        } catch (exception: Exception) {
            return MunroResult(error = IOException("Error getting data from network"))

        }
    }

}