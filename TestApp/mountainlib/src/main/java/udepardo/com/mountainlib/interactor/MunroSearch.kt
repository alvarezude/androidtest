package udepardo.com.mountainlib.interactor

import udepardo.com.mountainlib.*
import udepardo.com.mountainlib.model.BaseModel
import udepardo.com.mountainlib.model.MountainViewModel
import java.io.*


/**
 * Search param that needs to be provided to the search function.
 * With default values it'll return the full set of data
 */
data class SearchParam(
    val hillCategory: CATEGORY = CATEGORY.BOTH,
    val sort: SORTING = SORTING.ALPHABETICALLY,
    val sortOrder: ORDER = ORDER.ASC,
    val minHeight: Float = 0f,
    val maxHeight: Float = Float.MAX_VALUE,
    val maxResults: Int = 0
)


class MunroSearch(
    executor: Executor = DefaultThreadPoolExecutor,
    private val inputData: List<BaseModel>
) : MunroBaseInteractor<SearchParam, List<MountainViewModel>>(executor) {

    @Throws(IOException::class, IllegalArgumentException::class)
    override fun asyncCall(params: SearchParam?): List<MountainViewModel> {
        if (params == null) {
            throw IllegalArgumentException()
        }

        val result = inputData.filter(params.hillCategory)
            .sortBy(params.sort, params.sortOrder)
            .filter(params.minHeight, params.maxHeight)
            .map { MountainViewModel.Mapper.from(it) }
        return if (params.maxResults > 0){
            result.take(params.maxResults)
        }else{
            result
        }

    }
}
