package udepardo.com.mountainlib

import udepardo.com.mountainlib.model.BaseModel
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStream
import java.io.InputStreamReader


object Utils {
    /**
     * Reader from stream to a list of base model
     */
    @Throws(IOException::class)
    fun readStream(inputStream: InputStream): List<BaseModel> {
        val result = ArrayList<BaseModel>()
        var reader: BufferedReader? = null
        try {
            reader = BufferedReader(InputStreamReader(inputStream))
            var line = reader.readLine()
            while (line != null) {
                BaseModel.fromCVSRow(line)?.apply {
                    result.add(this)
                }
                line = reader.readLine()
            }
        } catch (e: IOException) {
            e.printStackTrace()
        } finally {
            try {
                reader?.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }
            return result
        }
    }
}