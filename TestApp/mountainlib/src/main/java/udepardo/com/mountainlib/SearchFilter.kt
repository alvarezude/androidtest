package udepardo.com.mountainlib

import udepardo.com.mountainlib.model.BaseModel

/**
 * For sorting, order and all the rest, i decided to use kotlin function extensions,
 * This adds to List<BaseModel> some functions.
 * The advantage of this approach is that all this functions return the same object so we can concatenate some of them
 */


enum class CATEGORY(val filter: (BaseModel) -> Boolean) {
    MUN({ it.yNow.equals("MUN", true) }),
    TOP({ it.yNow.equals("TOP", true) }),
    BOTH({ true })
}

enum class SORTING(val comparator: Comparator<BaseModel>
) {
    HEIGHT(
        Comparator { a, b -> a.heightMt.compareTo(b.heightMt) }),
    ALPHABETICALLY(
        Comparator { a, b -> a.name.compareTo(b.name) })
}

enum class ORDER {
    ASC, DESC
}


fun List<BaseModel>.filter(category: CATEGORY): List<BaseModel> {
    return this.filter(category.filter)
}

fun List<BaseModel>.filter(minHeight: Float = 0f, maxHeight: Float = Float.MAX_VALUE): List<BaseModel> {
    if (minHeight > maxHeight) {
        throw IllegalArgumentException("minValue cannot be greater than maxValue")
    }
    return this.filter { it.heightMt in minHeight..maxHeight }
}

fun List<BaseModel>.sortBy(sortby: SORTING, order: ORDER = ORDER.ASC) =
    if (order == ORDER.ASC){
        this.sortedWith(sortby.comparator)
    }else{
        this.sortedWith(sortby.comparator).reversed()
    }


