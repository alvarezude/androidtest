package udepardo.com.mountainlib

import udepardo.com.mountainlib.interactor.*
import udepardo.com.mountainlib.model.BaseModel
import udepardo.com.mountainlib.model.MountainViewModel


/**
 * The base Search.
 * MountainSearch and TestSearch extend this class, so we have the same behaviour for both and we can implement unit testing
 */
abstract class BaseSearch(
    private val baseExecutor: Executor = DefaultThreadPoolExecutor,
    private val baseCallback: (List<MountainViewModel>) -> Unit) {

    abstract fun getData(): List<BaseModel>

    fun searchValues(
        hillCategory: CATEGORY = CATEGORY.BOTH,
        sort: SORTING = SORTING.ALPHABETICALLY,
        sortOrder: ORDER = ORDER.ASC,
        minHeight: Float = 0f,
        maxHeight: Float = Float.MAX_VALUE,
        maxResults: Int = 0
    ) {


        MunroSearch(baseExecutor, getData()).execute(
            SearchParam(
                hillCategory,
                sort,
                sortOrder,
                minHeight,
                maxHeight,
                maxResults
            )
        ) {
            baseCallback.invoke(it)
        }

    }

}



