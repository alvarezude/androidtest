package udepardo.com.mountainlib.interactor

import udepardo.com.mountainlib.repository.LocalRepo
import udepardo.com.mountainlib.repository.LocalRepository
import udepardo.com.mountainlib.repository.RemoteRepo


/**
 * The network updater. It'll get the data from the remote repository and save it to the local repository
 */
class MunroNetworkUpdater(executor: Executor = DefaultThreadPoolExecutor,
                          private val localRepository: LocalRepo,
                          private val remoteRepo: RemoteRepo
                          ): MunroBaseInteractor<Unit, MunroResult<Boolean>>(executor) {
    override fun asyncCall(params: Unit?) : MunroResult<Boolean> {
        val remoteResult = remoteRepo.getMountains()
        return if (remoteResult.data != null){
            val localResult = localRepository.saveMountains(remoteResult.data)
            if (localResult.data != null){
                MunroResult(true, null)
            }else{
                MunroResult(false, localResult.error)
            }

        }else{
            MunroResult(false, remoteResult.error)
        }
    }
}
