package udepardo.com.mountainlib.model

/**
 * The output model request
 * The idea behind this is that we have the base model, we filter/search using it and then we map it to this model.
 * We have a Mapper with a parser function
 */
data class MountainViewModel(val name: String, val height: Float, val reference: String) {
    object Mapper {
        fun from(item: BaseModel) = MountainViewModel(item.name, item.heightMt, item.gridRef)
    }
}