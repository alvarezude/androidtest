package udepardo.com.mountainlib

import android.content.Context
import udepardo.com.mountainlib.interactor.MunroNetworkUpdater
import udepardo.com.mountainlib.model.BaseModel
import udepardo.com.mountainlib.model.MountainViewModel
import udepardo.com.mountainlib.repository.LocalRepository
import udepardo.com.mountainlib.repository.RemoteRepository

/**
 * The Search that should be used
 */
class MountainSearch(
    private val context: Context,
    private val forceUpdate: Boolean,
    callback: (List<MountainViewModel>) -> Unit
) : BaseSearch(baseCallback = callback) {

    fun init(initializationCallbacks: (Boolean) -> Unit) {
        if ((LocalRepository(context).getMountains().error != null) || forceUpdate) {
            MunroNetworkUpdater(
                localRepository = LocalRepository(context),
                remoteRepo = RemoteRepository()
            ).execute { result ->
                result.data?.let {
                    initializationCallbacks.invoke(it)
                } ?: initializationCallbacks.invoke(false)
            }
        }

    }

    override fun getData(): List<BaseModel> {
        return LocalRepository(context).getMountains().data?.let {
            it
        } ?: listOf()
    }
}