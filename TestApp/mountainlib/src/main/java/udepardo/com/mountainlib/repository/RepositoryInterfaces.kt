package udepardo.com.mountainlib.repository

import udepardo.com.mountainlib.interactor.MunroResult
import udepardo.com.mountainlib.model.BaseModel

/**
 * The remote interface. We can read mountains from it
 */
interface RemoteRepo {
    fun getMountains(): MunroResult<List<BaseModel>>
}

/**
 * The local interface. We can read mountains (as in the remote repositoty) as well to store them
 */
interface LocalRepo: RemoteRepo {
    fun saveMountains(data: List<BaseModel>): MunroResult<Unit>
}