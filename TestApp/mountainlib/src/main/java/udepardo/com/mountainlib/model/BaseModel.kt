package udepardo.com.mountainlib.model

import android.util.Log
import java.io.*
import kotlin.collections.ArrayList

/**
 * This model will mimic the structure of the csv.
 * I prefer to store all the data and then play with it instead of doing the filter here
 */
data class BaseModel(
    val runningNo: String = "",
    val doBIHNumber: String = "",
    val streetMap: String = "",
    val geograph: String = "",
    val hillBagging: String = "",
    val name: String = "",
    val smcSection: String = "",
    val rhbSection: String = "",
    val section: String = "",
    val heightMt: Float = 0f,
    val heightFt: Float = 0f,
    val map1_50: String, //TODO: Pair = "!
    val map1_25: String = "",
    val gridRef: String = "",
    val gridRefXY: String = "",
    val xCoord: String = "",
    val yCoord: String = "",
    val y1891: String = "",
    val y1921: String = "",
    val y1933: String = "",
    val y1953: String = "",
    val y1969: String = "",
    val y1974: String = "",
    val y1981: String = "",
    val y1984: String = "",
    val y1990: String = "",
    val y1997: String = "",
    val yNow: String = "",
    val comments: String = ""
) : Serializable {
    /**
     * The Parser can parse a comma separated value row into an object
     */
    companion object Parser {
        fun fromCVSRow(inputRow: String): BaseModel? {
            //We filter that the line is a mountain and it's well formed. We discard headers and footers as well as the ones with wrong height
            parseLine(inputRow).apply {
                if ((this.size != 29) || (this[27].isEmpty()) || !this[9].matches(Regex("\\d+(?:\\.\\d+)?")) || !this[10].matches(Regex("\\d+(?:\\.\\d+)?"))) {
                    Log.e("MUNRO", "Discarding $inputRow ")
                    return null
                } else {
                    return BaseModel(
                        this[0],
                        this[1],
                        this[2],
                        this[3],
                        this[4],
                        this[5],
                        this[6],
                        this[7],
                        this[8],
                        this[9].toFloat(),
                        this[10].toFloat(),
                        this[11],
                        this[12],
                        this[13],
                        this[14],
                        this[15],
                        this[16],
                        this[17],
                        this[18],
                        this[19],
                        this[20],
                        this[21],
                        this[22],
                        this[23],
                        this[24],
                        this[25],
                        this[26],
                        this[27],
                        if (this[28] == null) {
                            ""
                        } else this[28]
                    )
                }
            }

        }

        /**
         * Parses a line
         * Stringtokenizer or split by commas won't work becase the urls and the comments contain commas
         * @return a list of 29 strings that we'll have to match with the BaseModel
         */
        private fun parseLine(input: String): ArrayList<String> {
            val result = ArrayList<String>(29)
            var currentString = ""
            var enabledCommas = true

            input.forEachIndexed { index, c ->
                when (c) {
                    '"' -> enabledCommas = !enabledCommas
                    ',' -> if (enabledCommas) {
                        result.add(currentString)
                        currentString = ""
                    } else {
                        currentString += c
                    }

                    else -> {
                        currentString += c
                    }
                }
                if (result.size == 28) {
                    enabledCommas = false
                }
                if (index == input.length - 1) {
                    result.add(currentString)
                    currentString = ""
                }
            }
            return result
        }
    }
}







