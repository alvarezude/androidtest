package udepardo.com.mountainlib

import udepardo.com.mountainlib.interactor.TestingExecutor
import udepardo.com.mountainlib.model.BaseModel
import udepardo.com.mountainlib.model.MountainViewModel



/**
 * The Search only for testing
 */
class TestSearch(
    private val testData: List<BaseModel>,
    callback: (List<MountainViewModel>) -> Unit
) : BaseSearch(baseCallback = callback, baseExecutor = TestingExecutor ) {

    override fun getData() = testData

}