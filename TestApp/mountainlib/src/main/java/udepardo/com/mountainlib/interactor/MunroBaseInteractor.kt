package udepardo.com.mountainlib.interactor

import android.os.Handler
import android.os.Looper
import java.lang.Exception
import java.util.concurrent.ExecutorService
import java.util.concurrent.LinkedBlockingQueue
import java.util.concurrent.ThreadPoolExecutor
import java.util.concurrent.TimeUnit



/**
 * Data class that will be used as result wrapper
 */
data class MunroResult<T>(val data: T? = null, val error: Exception? = null)

/**
 * The base interactor: We use generics for input and output parameters.
 * To create an interactor we need to override the asyncCall function. The code inside that function will be executed in the executor
 * and returned in the main thread
 */
abstract class MunroBaseInteractor<in U, out T>(private val executor: Executor = DefaultThreadPoolExecutor) {
    abstract fun asyncCall(params: U? = null): T

    fun execute(params: U? = null, callback: (T) -> Unit) {
        runOnExecutor {
            val result = asyncCall(params)
            Handler(Looper.getMainLooper()).post { callback(result) }
        }
    }

    private fun runOnExecutor(function: () -> Unit) {
        executor.execute(Runnable {
            function()
        })
    }
}

/**
 * the idea is being able to inject the executor to the interactors. This way we can make the tests being synchronous by
 * creating the interactor with a "Testing Executor". If no interactor is provided as paramn, it'll use the "DefaultThreadPoolExecutor" that is
 * a normal pool of threads
 */
interface Executor{
    fun execute(r: Runnable)
}

/**
 * The executor for tests. Basically won't change thread to allow unit testing. Will execute synchronously
 */
object TestingExecutor : Executor {
    override fun execute(r: Runnable) {
        r.run()
    }
}

/**
 * The default executor. It'll execute the runnable inside a threadpool
 */
object DefaultThreadPoolExecutor : Executor {
    /**
     * Time to keep an idle thread if the size has expired.
     */
    private val KEEP_ALIVE_TIME = 1L

    /**
     * The number unit in seconds to wait.
     */
    private val KEEP_ALIVE_TIME_UNIT = TimeUnit.SECONDS

    /**
     * The number of cores available.
     */
    private val NUMBER_OF_CORES = Runtime.getRuntime().availableProcessors()

    /**
     * The number of cores.
     */
    private val CORE_POOL_SIZE = NUMBER_OF_CORES + 1

    /**
     * The maximum number of processes running in the pool.
     */
    private val MAXIMUM_POOL_SIZE = NUMBER_OF_CORES * 2 + 1

    /**
     * The pool threadPolicy.
     */
    private val poolQueue: ExecutorService = ThreadPoolExecutor(
        CORE_POOL_SIZE,
        MAXIMUM_POOL_SIZE,
        KEEP_ALIVE_TIME,
        KEEP_ALIVE_TIME_UNIT, LinkedBlockingQueue()
    )

    override fun execute(runnable: Runnable) {
        poolQueue.submit(runnable)
    }
}