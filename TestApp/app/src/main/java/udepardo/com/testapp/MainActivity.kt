package udepardo.com.testapp

import android.app.Activity
import android.os.Bundle
import android.widget.Toast
import udepardo.com.mountainlib.*
import udepardo.com.mountainlib.model.MountainViewModel


/**
 * This is just a test activity to do some testing on the library
 */
class MainActivity : Activity() {
lateinit var search: MountainSearch

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        search = MountainSearch(this, true, ::proccessResult).apply {
            init {initialized ->
                if (initialized){
                    start()
                }else{
                    failedInit()
                }

            }
        }
        search.searchValues()

    }

    fun start(){
        search.searchValues()
    }

    private fun failedInit(){
        Toast.makeText(this, "Error initializing file", Toast.LENGTH_SHORT).show()
    }

    private fun proccessResult(list: List<MountainViewModel>){
        Toast.makeText(this, "Size ${list.size}", Toast.LENGTH_SHORT).show()

    }









}
