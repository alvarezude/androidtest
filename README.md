# Ain't no mountain higher lib

Library to read/store mountains from a csv.

Notes:

- I uploaded the cvs to my heroku account. I did that to make the file easily updateable. 
- Probably more tests could be implemented and the ability of search by 2 criteria could probably be done using sortedWith

The library includes a test that can check the file in assets and check if the parser works with that file.


